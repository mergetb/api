// +build tools

package tools

import (
	_ "github.com/golang/protobuf/protoc-gen-go"
	_ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway"
	_ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2"
	_ "github.com/stormcat24/protodep"
	//_ "google.golang.org/grpc/cmd/protoc-gen-go-grpc"
	//_ "google.golang.org/protobuf/cmd/protoc-gen-go"
)

// The mergetb subgroup patch. This can be removed once it's merged into master.
replace github.com/stormcat24/protodep ==> github.com/stormcat24/protodep@401743f14904dcf0a6e60b23ba1d603c1d0af8d6
