package facility

import (
	"encoding/binary"
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
)

func (n *DanceNetwork) Addrv4() net.IP {

	buf := []byte{0, 0, 0, 0}
	binary.BigEndian.PutUint32(buf, n.DanceAddrV4)
	return net.IP(buf)

}

func (n *DanceNetwork) Addrv6() net.IP {

	buf := []byte{
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
	}
	copy(buf[:], n.DanceAddrV6)
	return net.IP(buf)

}

func (n *DanceNetwork) GatewayAsIP() net.IP {

	buf := []byte{0, 0, 0, 0}
	binary.BigEndian.PutUint32(buf, n.Gateway)
	return net.IP(buf)

}

func (n *DanceNetwork) SubnetMaskAsIPMask() net.IPMask {

	return net.CIDRMask(int(n.SubnetMask), 32)

}

func (n *DanceNetwork) SetAddrFromCIDRV4(addr string) error {

	ip, _, err := net.ParseCIDR(addr)
	if err != nil {
		return fmt.Errorf("invalid ip %s", addr)
	}

	n.DanceAddrV4 = binary.BigEndian.Uint32([]byte(ip.To4()))

	return nil

}

func (n *DanceNetwork) SetGatewayFromCIDR(addr string) error {

	ip, _, err := net.ParseCIDR(addr)
	if err != nil {
		return fmt.Errorf("invalid ip %s", addr)
	}

	n.Gateway = binary.BigEndian.Uint32([]byte(ip.To4()))

	return nil

}

func (e *DanceEntry) SetIPv4FromCIDRString(addr string) error {

	log.Printf("PREFIX STRING: %s", addr)

	ip, _, err := net.ParseCIDR(addr)
	if err != nil {
		return fmt.Errorf("invalid ip %s", addr)
	}

	e.Ipv4 = binary.BigEndian.Uint32([]byte(ip.To4()))

	log.Printf("PREFIX INT: %d", e.Ipv4)

	return nil

}

func (d4 *DanceDns4Entry) AddAddrs(addrs ...net.IP) {

	for _, x := range addrs {
		d4.Ipv4 = append(d4.Ipv4, binary.BigEndian.Uint32([]byte(x.To4())))
	}

}
