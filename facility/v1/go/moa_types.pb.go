// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.31.0
// 	protoc        (unknown)
// source: facility/v1/moa_types.proto

package facility

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type SetThreadsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *SetThreadsResponse) Reset() {
	*x = SetThreadsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_facility_v1_moa_types_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SetThreadsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SetThreadsResponse) ProtoMessage() {}

func (x *SetThreadsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_facility_v1_moa_types_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SetThreadsResponse.ProtoReflect.Descriptor instead.
func (*SetThreadsResponse) Descriptor() ([]byte, []int) {
	return file_facility_v1_moa_types_proto_rawDescGZIP(), []int{0}
}

type GetThreadsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Mzid string `protobuf:"bytes,1,opt,name=mzid,proto3" json:"mzid,omitempty"` // optional. empty value will return all thread configuration
}

func (x *GetThreadsRequest) Reset() {
	*x = GetThreadsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_facility_v1_moa_types_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetThreadsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetThreadsRequest) ProtoMessage() {}

func (x *GetThreadsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_facility_v1_moa_types_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetThreadsRequest.ProtoReflect.Descriptor instead.
func (*GetThreadsRequest) Descriptor() ([]byte, []int) {
	return file_facility_v1_moa_types_proto_rawDescGZIP(), []int{1}
}

func (x *GetThreadsRequest) GetMzid() string {
	if x != nil {
		return x.Mzid
	}
	return ""
}

type SetThreadsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Mzid    string `protobuf:"bytes,1,opt,name=mzid,proto3" json:"mzid,omitempty"`
	Threads uint32 `protobuf:"varint,2,opt,name=threads,proto3" json:"threads,omitempty"` // a value of 0 removes the thread config
}

func (x *SetThreadsRequest) Reset() {
	*x = SetThreadsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_facility_v1_moa_types_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SetThreadsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SetThreadsRequest) ProtoMessage() {}

func (x *SetThreadsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_facility_v1_moa_types_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SetThreadsRequest.ProtoReflect.Descriptor instead.
func (*SetThreadsRequest) Descriptor() ([]byte, []int) {
	return file_facility_v1_moa_types_proto_rawDescGZIP(), []int{2}
}

func (x *SetThreadsRequest) GetMzid() string {
	if x != nil {
		return x.Mzid
	}
	return ""
}

func (x *SetThreadsRequest) GetThreads() uint32 {
	if x != nil {
		return x.Threads
	}
	return 0
}

type GetThreadsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Req []*SetThreadsRequest `protobuf:"bytes,1,rep,name=req,proto3" json:"req,omitempty"`
}

func (x *GetThreadsResponse) Reset() {
	*x = GetThreadsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_facility_v1_moa_types_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetThreadsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetThreadsResponse) ProtoMessage() {}

func (x *GetThreadsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_facility_v1_moa_types_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetThreadsResponse.ProtoReflect.Descriptor instead.
func (*GetThreadsResponse) Descriptor() ([]byte, []int) {
	return file_facility_v1_moa_types_proto_rawDescGZIP(), []int{3}
}

func (x *GetThreadsResponse) GetReq() []*SetThreadsRequest {
	if x != nil {
		return x.Req
	}
	return nil
}

var File_facility_v1_moa_types_proto protoreflect.FileDescriptor

var file_facility_v1_moa_types_proto_rawDesc = []byte{
	0x0a, 0x1b, 0x66, 0x61, 0x63, 0x69, 0x6c, 0x69, 0x74, 0x79, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x6f,
	0x61, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0b, 0x66,
	0x61, 0x63, 0x69, 0x6c, 0x69, 0x74, 0x79, 0x2e, 0x76, 0x31, 0x22, 0x14, 0x0a, 0x12, 0x53, 0x65,
	0x74, 0x54, 0x68, 0x72, 0x65, 0x61, 0x64, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x27, 0x0a, 0x11, 0x47, 0x65, 0x74, 0x54, 0x68, 0x72, 0x65, 0x61, 0x64, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6d, 0x7a, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x04, 0x6d, 0x7a, 0x69, 0x64, 0x22, 0x41, 0x0a, 0x11, 0x53, 0x65, 0x74,
	0x54, 0x68, 0x72, 0x65, 0x61, 0x64, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12,
	0x0a, 0x04, 0x6d, 0x7a, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6d, 0x7a,
	0x69, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x74, 0x68, 0x72, 0x65, 0x61, 0x64, 0x73, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x0d, 0x52, 0x07, 0x74, 0x68, 0x72, 0x65, 0x61, 0x64, 0x73, 0x22, 0x46, 0x0a, 0x12,
	0x47, 0x65, 0x74, 0x54, 0x68, 0x72, 0x65, 0x61, 0x64, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x30, 0x0a, 0x03, 0x72, 0x65, 0x71, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32,
	0x1e, 0x2e, 0x66, 0x61, 0x63, 0x69, 0x6c, 0x69, 0x74, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x53, 0x65,
	0x74, 0x54, 0x68, 0x72, 0x65, 0x61, 0x64, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52,
	0x03, 0x72, 0x65, 0x71, 0x42, 0x30, 0x5a, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63,
	0x6f, 0x6d, 0x2f, 0x6d, 0x65, 0x72, 0x67, 0x65, 0x74, 0x62, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x66,
	0x61, 0x63, 0x69, 0x6c, 0x69, 0x74, 0x79, 0x2f, 0x76, 0x31, 0x2f, 0x67, 0x6f, 0x3b, 0x66, 0x61,
	0x63, 0x69, 0x6c, 0x69, 0x74, 0x79, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_facility_v1_moa_types_proto_rawDescOnce sync.Once
	file_facility_v1_moa_types_proto_rawDescData = file_facility_v1_moa_types_proto_rawDesc
)

func file_facility_v1_moa_types_proto_rawDescGZIP() []byte {
	file_facility_v1_moa_types_proto_rawDescOnce.Do(func() {
		file_facility_v1_moa_types_proto_rawDescData = protoimpl.X.CompressGZIP(file_facility_v1_moa_types_proto_rawDescData)
	})
	return file_facility_v1_moa_types_proto_rawDescData
}

var file_facility_v1_moa_types_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_facility_v1_moa_types_proto_goTypes = []interface{}{
	(*SetThreadsResponse)(nil), // 0: facility.v1.SetThreadsResponse
	(*GetThreadsRequest)(nil),  // 1: facility.v1.GetThreadsRequest
	(*SetThreadsRequest)(nil),  // 2: facility.v1.SetThreadsRequest
	(*GetThreadsResponse)(nil), // 3: facility.v1.GetThreadsResponse
}
var file_facility_v1_moa_types_proto_depIdxs = []int32{
	2, // 0: facility.v1.GetThreadsResponse.req:type_name -> facility.v1.SetThreadsRequest
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_facility_v1_moa_types_proto_init() }
func file_facility_v1_moa_types_proto_init() {
	if File_facility_v1_moa_types_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_facility_v1_moa_types_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SetThreadsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_facility_v1_moa_types_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetThreadsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_facility_v1_moa_types_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SetThreadsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_facility_v1_moa_types_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetThreadsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_facility_v1_moa_types_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_facility_v1_moa_types_proto_goTypes,
		DependencyIndexes: file_facility_v1_moa_types_proto_depIdxs,
		MessageInfos:      file_facility_v1_moa_types_proto_msgTypes,
	}.Build()
	File_facility_v1_moa_types_proto = out.File
	file_facility_v1_moa_types_proto_rawDesc = nil
	file_facility_v1_moa_types_proto_goTypes = nil
	file_facility_v1_moa_types_proto_depIdxs = nil
}
