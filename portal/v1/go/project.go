package portal

func (p *Project) HasMember(member string) bool {

	for username, _ := range p.Members {
		if username == member {
			return true
		}
	}

	return false

}
