package portal

import (
	"bytes"
	"context"
	"fmt"
	//"reflect"
)

const (
	buf_sz    = 64 << 10 // for better performance keep > than max chunk size
	max_chunk = 32 << 10
)

type GrpcByteStreamSender interface {
	Context() context.Context
	Send([]byte) (int, error)
}

type GrpcByteStreamReceiver interface {
	Context() context.Context
	Recv() ([]byte, error)
}

type GrpcByteStreamReader struct {
	stream GrpcByteStreamReceiver
	buffer *bytes.Buffer
}

type GrpcByteStreamWriter struct {
	stream GrpcByteStreamSender
	buffer *bytes.Buffer
}

func NewGrpcByteStreamReader(stream GrpcByteStreamReceiver) *GrpcByteStreamReader {
	buf := &bytes.Buffer{}
	buf.Grow(buf_sz)

	// one of recv/send may not be found, but that's ok, assuming this is used correctly
	return &GrpcByteStreamReader{
		stream: stream,
		buffer: buf,
	}
}

func NewGrpcByteStreamWriter(stream GrpcByteStreamSender) *GrpcByteStreamWriter {
	buf := &bytes.Buffer{}
	buf.Grow(buf_sz)

	// one of recv/send may not be found, but that's ok, assuming this is used correctly
	return &GrpcByteStreamWriter{
		stream: stream,
		buffer: buf,
	}
}

// GrpcByteStreamReader.Read is a buffered read from the stream
func (r *GrpcByteStreamReader) Read(p []byte) (n int, err error) {
	ctx := r.stream.Context()
	select {
	case <-ctx.Done():
		if err := ctx.Err(); err != nil {
			return 0, err
		}
	default:
	}
	b, err := r.stream.Recv()
	if err != nil {
		return 0, err
	}
	_, err = r.buffer.Write(b)
	if err != nil {
		return 0, err
	}
	return r.buffer.Read(p)
}

// GrpcByteStreamWriter.Write is a buffered write to the stream
func (w *GrpcByteStreamWriter) Write(p []byte) (n int, err error) {
	n = len(p)
	_, err = w.buffer.Write(p)
	if err != nil {
		return 0, err
	}
	ctx := w.stream.Context()
	// read and send one chunk at a time
	// the loop is largely unnecessary because the grpc max message is very large,
	// so keeping chunks under max_chunk is just for corner cases
	b := make([]byte, max_chunk)
	for {
		select {
		case <-ctx.Done():
			if err := ctx.Err(); err != nil {
				return 0, err
			}
		default:
		}
		sz, err := w.buffer.Read(b)
		if err != nil {
			return 0, err
		}
		l, err := w.stream.Send(b[:sz])
		if err != nil {
			return 0, err
		}
		if l != sz {
			return 0, fmt.Errorf("short send")
		}
		if w.buffer.Len() == 0 {
			break
		}
	}
	return
}
