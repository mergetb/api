package portal

import (
	"fmt"

	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/mergetb/tech/reconcile"
)

func NewTaskSummaryError(err error) *TaskSummary {
	return &TaskSummary{
		HighestStatus: TaskStatus_Error,
		LastUpdated:   timestamppb.Now(),
		Messages: []*TaskMessage{
			{
				Level:   TaskMessage_Error,
				Message: err.Error(),
			},
		},
	}
}

func (ts *TaskStatus) JoinTaskTrees(tts ...*TaskTree) *TaskTree {
	root := new(TaskTree)
	root.Task = ts

	for _, tt := range tts {
		if tt == nil {
			continue
		}

		if tt.HighestStatus > root.HighestStatus {
			root.HighestStatus = tt.HighestStatus
			ts.StatusValue = tt.HighestStatus
		}

		if tt.Task.FirstUpdated != nil &&
			(ts.FirstUpdated == nil || tt.Task.FirstUpdated.AsTime().Before(ts.FirstUpdated.AsTime())) {

			ts.FirstUpdated = tt.Task.FirstUpdated
		}

		if tt.LastUpdated != nil &&
			(root.LastUpdated == nil || tt.LastUpdated.AsTime().After(root.LastUpdated.AsTime())) {

			ts.LastUpdated = tt.LastUpdated
			root.LastUpdated = tt.LastUpdated
		}

		root.NumChildTasks += tt.NumChildTasks + 1
		root.Subtasks = append(root.Subtasks, tt)
	}

	return root

}

func (ts *TaskSummary) Merge(other ...*TaskSummary) {
	if ts == nil {
		switch len(other) {
		case 0:
			// do nothing
		case 1:
			ts = other[0]
		default:
			ts = other[0]
			ts.Merge(other[1:]...)
		}

		return
	}

	for _, o := range other {
		if o == nil {
			continue
		}

		if o.HighestStatus > ts.HighestStatus {
			ts.HighestStatus = o.HighestStatus
		}

		if o.LastUpdated != nil && o.LastUpdated.AsTime().After(ts.LastUpdated.AsTime()) {
			ts.LastUpdated = o.LastUpdated
		}

		if (ts.FirstUpdated == nil) ||
			(o.FirstUpdated != nil && ts.FirstUpdated.AsTime().After(o.FirstUpdated.AsTime())) {
			ts.FirstUpdated = o.FirstUpdated
		}

		ts.Messages = append(ts.Messages, o.Messages...)
	}

}

func (tt *TaskTree) ToTaskSummary() *TaskSummary {
	if tt == nil {
		return nil
	}

	return &TaskSummary{
		HighestStatus: tt.HighestStatus,
		FirstUpdated:  tt.Task.FirstUpdated,
		LastUpdated:   tt.LastUpdated,
		Messages:      tt.getUnsuccessfulStatuses(nil),
	}
}

func (tt *TaskTree) getUnsuccessfulStatuses(acc []*TaskMessage) []*TaskMessage {
	if tt == nil {
		return acc
	}

	if tt.Task != nil && tt.Task.Message != nil && tt.Task.Message.Level <= TaskMessage_Warning {
		acc = append(acc, &TaskMessage{
			Level: tt.Task.Message.Level,
			Message: fmt.Sprintf(
				"%s: %s",
				tt.Task.Name,
				tt.Task.Message.Message,
			),
		})
	}

	for _, st := range tt.Subtasks {
		acc = st.getUnsuccessfulStatuses(acc)
	}

	return acc
}

func NewTaskTreeFromReconcileForest(tf *reconcile.TaskForest) (tt *TaskTree) {
	tt = new(TaskTree)
	tt.fromReconcileForest(tf)

	return
}

func (tt *TaskTree) fromReconcileForest(tf *reconcile.TaskForest) {
	if tt == nil {
		return
	}

	if tt.Task == nil {
		tt.Task = new(TaskStatus)
	}

	// fill in the task
	tt.Task.fromReconcileTaskForest(tf)

	// fill in the associated data
	tt.HighestStatus.fromReconcileStatusType(tf.HighestStatus)
	tt.LastUpdated = tf.LastUpdated
	tt.NumChildTasks = tf.NumChildTasks

	// fill in the subtasks
	for _, sg := range tf.Subgoals {
		t := new(TaskTree)
		t.fromReconcileForest(sg)

		tt.Subtasks = append(tt.Subtasks, t)
	}
	for _, st := range tf.Subtasks {
		t := new(TaskTree)
		t.fromReconcileTree(st)

		tt.Subtasks = append(tt.Subtasks, t)
	}
}

func (tt *TaskTree) fromReconcileTree(t *reconcile.TaskTree) {
	if t == nil {
		return
	}

	if tt.Task == nil {
		tt.Task = new(TaskStatus)
	}

	// fill in the task
	tt.Task.fromReconcileTaskTree(t)

	// fill in the associated data
	tt.HighestStatus.fromReconcileStatusType(t.HighestStatus)
	tt.LastUpdated = t.LastUpdated
	tt.NumChildTasks = t.NumChildTasks

	// fill in the subtasks
	for _, st := range t.Subtasks {
		t := new(TaskTree)
		t.fromReconcileTree(st)

		t.Subtasks = append(t.Subtasks, t)
	}
}

func (sv *TaskStatus_StatusType) fromReconcileStatusType(s reconcile.TaskStatus_StatusType) {
	switch s {
	case reconcile.TaskStatus_Undefined:
		*sv = TaskStatus_Undefined
	case reconcile.TaskStatus_Success:
		*sv = TaskStatus_Success
	case reconcile.TaskStatus_Deleted:
		*sv = TaskStatus_Deleted
	case reconcile.TaskStatus_Pending:
		*sv = TaskStatus_Pending
	case reconcile.TaskStatus_Unresponsive:
		*sv = TaskStatus_Unresponsive
	case reconcile.TaskStatus_Processing:
		*sv = TaskStatus_Processing
	case reconcile.TaskStatus_Error:
		*sv = TaskStatus_Error
	default:
		panic(fmt.Errorf("unknown reconcile status type: %s", s))
	}

}

func (ts *TaskStatus) fromReconcileTaskForest(tf *reconcile.TaskForest) {
	if tf == nil {
		return
	}

	// it may not have a real selfkey, if it's emphemeral
	ts.Id = tf.Goal.SelfKey
	if ts.Id == "" {
		ts.Id = tf.Goal.Name
	}

	ts.Name = tf.Goal.Name
	ts.Message = &TaskMessage{
		Level:   TaskMessage_Info,
		Message: tf.Goal.Desc,
	}
	ts.FirstUpdated = tf.Goal.Creation
	ts.LastUpdated = tf.LastUpdated
	ts.StatusValue.fromReconcileStatusType(tf.HighestStatus)
}

func (ts *TaskStatus) fromReconcileTaskTree(tt *reconcile.TaskTree) {
	ts.Id = tt.Task.TaskKey
	// XXX: an actual name, probably have to extend the reconcile actions
	ts.Name = tt.Task.TaskKey
	ts.FirstUpdated = tt.Task.Creation
	ts.LastUpdated = tt.Task.When
	ts.StatusValue.fromReconcileStatusType(tt.Task.CurrentStatus)

	level := TaskMessage_Undefined
	message := ""

	for _, tm := range tt.Task.Messages {
		new_level := TaskMessage_Trace

		switch tm.Level {
		case reconcile.TaskMessage_Undefined:
			new_level = TaskMessage_Undefined
		case reconcile.TaskMessage_Error:
			new_level = TaskMessage_Error
		case reconcile.TaskMessage_Warning:
			new_level = TaskMessage_Warning
		case reconcile.TaskMessage_Info:
			new_level = TaskMessage_Info
		case reconcile.TaskMessage_Debug:
			new_level = TaskMessage_Debug
		case reconcile.TaskMessage_Trace:
			new_level = TaskMessage_Trace
		default:
			panic(fmt.Errorf("unknown reconcile message type: %s", tm.Level))
		}

		if level == TaskMessage_Undefined || new_level < level {
			level = new_level
		}

		message += tm.Message + "\n"
	}

	if len(message) >= 1 {
		// remove last whitespace character
		message = message[:len(message)-1]
	}

	if message != "" {
		ts.Message = &TaskMessage{
			Level:   level,
			Message: message,
		}
	}
}
