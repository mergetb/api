package portal

import (
	"gitlab.com/mergetb/xir/v0.3/go"
)

func NewAllocationTable() *AllocationTable {
	return &AllocationTable{
		Resource: make(map[string]*ResourceAllocationList),
		Cable:    make(map[string]*CableAllocationList),
	}
}

func (a *AllocationTable) AllocateResource(id string, ac *xir.ResourceAllocation) {

	if a.Resource == nil {
		a.Resource = make(map[string]*ResourceAllocationList)
	}

	list, ok := a.Resource[id]
	if !ok {
		list = new(ResourceAllocationList)
		a.Resource[id] = list
	}
	list.Value = append(list.Value, ac)

}

func (a *AllocationTable) AllocateCable(id string, ac *xir.CableAllocation) {

	if a.Cable == nil {
		a.Cable = make(map[string]*CableAllocationList)
	}

	list, ok := a.Cable[id]
	if !ok {
		list = new(CableAllocationList)
		a.Cable[id] = list
	}
	list.Value = append(list.Value, ac)

}

func (a *AllocationTable) GetAllocation(host, node string) *xir.ResourceAllocation {

	resourceAllocationList, ok := a.Resource[host]
	if !ok {
		return nil
	}
	for _, x := range resourceAllocationList.Value {
		if x.Node == node {
			return x
		}
	}
	return nil

}
