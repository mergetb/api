package portal

import (
	"fmt"
)

// Next returns the nex available index.
func (cs CountSet) Next() (uint64, uint64) {

	i := cs.Offset
	for j, x := range cs.Values {
		if x != i {
			return i, uint64(j)
		}
		i++
	}
	return i, uint64(len(cs.Values))

}

// Add increments the counter and returns the created index value.
func (cs CountSet) Add() (uint64, CountSet, error) {

	if uint64(len(cs.Values)) == cs.Size {
		return 0, cs, fmt.Errorf("overflow")
	}
	i, j := cs.Next()
	cs.Values = append(cs.Values[:j], append([]uint64{i}, cs.Values[j:]...)...)
	return i, cs, nil

}

// Remove removes an index from the counter, freeing it for future use.
func (cs CountSet) Remove(i uint64) CountSet {
	for j, x := range cs.Values {
		if i == x {
			var tail []uint64
			if j < len(cs.Values)-1 {
				tail = cs.Values[j+1:]
			}
			cs.Values = append(cs.Values[:j], tail...)
			return cs
		}
	}
	return cs
}

// Key returns the datastore key for this object.
func (c *CountSet) Key() string { return "/counters/" + c.Name }

// GetVersion returns the current datastore version of the object
func (c *CountSet) GetVersion() int64 { return c.Ver }

// SetVersion sets the current datastore version of the object
func (c *CountSet) SetVersion(v int64) { c.Ver = v }

// Value returns this object as an interface{}
func (c *CountSet) Object() interface{} { return c }

func (c *CountSet) Merge(other interface{}) error { return nil }
