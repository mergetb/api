package portal

//import "fmt"

/*
	For testing if realizations are identical, it's quite useful to
	define our own set type, as the order of the endpoints and waypoints
	in a realization does not matter.

	This is slightly different from a regular set, as this will also
	count how many times a key has been added, so we can check if
	slices contain the same number of Hashables. You can use it as a regular
	set if you ignore the number of keys has been added.

	We couldn't use a map[*Waypoint|*Endpoint]struct{} type because
	Waypoints and Endpoints that we consider identical but with different pointers
	would be considered as unequal in the map, making it unusable as a set.

	In the future, this should use golang's generics implementation, as it's quite
	annoying to cast things back and forth from Hashable.
*/

type Hashable interface {
	Hash() uint32
	Equals(interface{}) bool
}

type SetKV struct {
	Key   Hashable
	Value int
}

type Set map[uint32][]SetKV

func (s Set) GetCount(key Hashable) int {
	if lists, ok := s[key.Hash()]; ok {
		for _, kv := range lists {
			if key.Equals(kv.Key) {
				return kv.Value
			}
		}
	}

	return 0
}

func (s Set) SetKey(key Hashable, n int) {
	if n <= 0 {
		s.Remove(key)
		return
	}

	if !s.Contains(key) {
		s.Add(key)
	}

	if lists, ok := s[key.Hash()]; ok {
		for i, kv := range lists {
			if key.Equals(kv.Key) {
				lists[i] = SetKV{kv.Key, n}
			}
		}
	}
}

func (s Set) ContainsAny(keys ...Hashable) bool {
	for _, key := range keys {
		if s.GetCount(key) > 0 {
			return true
		}
	}

	return false
}

func (s Set) Contains(key Hashable) bool {
	return s.GetCount(key) > 0
}

func (s Set) ContainsAll(keys ...Hashable) bool {
	for _, key := range keys {
		if !s.ContainsAny(key) {
			return false
		}
	}

	return true
}

func (s Set) AddAndGetCount(key Hashable) int {
	if !s.Contains(key) {
		s[key.Hash()] = append(s[key.Hash()], SetKV{key, 1})
		return 1
	} else {
		if lists, ok := s[key.Hash()]; ok {
			for i, kv := range lists {
				if key.Equals(kv.Key) {
					lists[i] = SetKV{kv.Key, kv.Value + 1}
					return lists[i].Value
				}
			}
		}
	}

	return 0
}

func (s Set) Add(key Hashable) bool {
	return s.AddAndGetCount(key) > 0
}

func (s Set) Remove(key Hashable) bool {
	found := false

	if lists, ok := s[key.Hash()]; ok {
		var new_list []SetKV

		for _, kv := range lists {
			if !key.Equals(kv.Key) {
				new_list = append(new_list, kv)
			} else {
				found = true
			}
		}

		s[key.Hash()] = new_list
	}

	return found
}

func (s Set) ToSlice() []Hashable {
	var keys []Hashable

	for _, list := range s {
		for _, kv := range list {
			keys = append(keys, kv.Key)
		}
	}

	return keys
}

func (s Set) Equals(other Set) bool {
	h1 := s.ToSlice()
	h2 := other.ToSlice()

	if len(h1) != len(h2) {
		return false
	}

	for _, key := range h1 {
		//fmt.Printf("%+v, %d, %d\n", key, s.GetCount(key), other.GetCount(key))
		if s.GetCount(key) != other.GetCount(key) {
			return false
		}
	}

	return true
}

func (s Set) EqualsIgnoreCount(other Set) bool {
	h1 := s.ToSlice()
	h2 := other.ToSlice()

	if len(h1) != len(h2) {
		return false
	}

	return s.ContainsAll(h2...)
}

/*
	This takes in:
	  - a slice of Hashables that we want to remove duplicates from
	  - a Set of that we don't want present in the result
	This returns:
	  - the original slice of Hashables with:
	    - duplicates removed
	  - any items in the set completely removed
	  - the original set with anything that we removed also added to the set

	The intent of this is to remove duplicates in one slice with:
		slice, _ = RemoveDuplicates(slice, nil)

	Or across multiple slices with:
		acc_removed = nil
		for i := range slices {
			slices[i], acc_removed = RemoveDuplicates(slices[i], acc_removed)
	}
*/
func RemoveDuplicates(list []Hashable, already_added Set) ([]Hashable, Set) {

	if already_added == nil {
		already_added = make(Set)
	}

	var new []Hashable

	for _, e := range list {

		if !already_added.Contains(e) {
			already_added.Add(e)
			new = append(new, e)
		}
	}

	return new, already_added
}

/*
	This checks if two []Hashable are identical.
	The order of the elements do not matter,
	but the amount of each duplicate element does matter.
*/
func HashSliceEquals(list, other []Hashable) bool {

	if len(list) != len(other) {
		return false
	}

	s1 := make(Set)

	for _, d := range list {
		s1.Add(d)
	}

	s2 := make(Set)

	for _, d := range other {
		s2.Add(d)
	}

	return s1.Equals(s2)
}
