package portal

import (
	"encoding/binary"
	"fmt"
	"hash/fnv"
	"log"
	"reflect"
	"sort"
	"strings"
)

type Fields map[string]string

type Diagnostics []*Diagnostic

func (ds Diagnostics) Error() bool {
	for _, x := range ds {
		if x.Level == DiagnosticLevel_Error {
			return true
		}
	}
	return false
}

func (dl DiagnosticList) Error() bool {

	return Diagnostics(dl.Value).Error()

}

func (ds Diagnostics) WithFields(fields Fields) Diagnostics {

	for _, d := range ds {
		for k, v := range fields {
			d.Data[k] = v
		}
	}

	return ds
}

type AggregateDiagnostic struct {
	Hosts      map[string]struct{}
	Diagnostic *Diagnostic
}

func (dl DiagnosticList) ToString() string {

	return Diagnostics(dl.Value).ToString()

}

func (ds Diagnostics) ToString() string {

	withguest := make(map[string]map[string]*AggregateDiagnostic)
	var nameless []*Diagnostic

	for _, d := range ds {
		if d.Guest == "" {
			nameless = append(nameless, d)
			continue
		}

		_, ok := withguest[d.Guest]
		if !ok {
			withguest[d.Guest] = make(map[string]*AggregateDiagnostic)
		}
		_, ok = withguest[d.Guest][d.Message]
		if !ok {
			h := make(map[string]struct{})
			h[d.Host] = struct{}{}

			withguest[d.Guest][d.Message] = &AggregateDiagnostic{
				Hosts:      h,
				Diagnostic: d,
			}
		} else {
			if d.Host != "" {
				withguest[d.Guest][d.Message].Hosts[d.Host] = struct{}{}
			}
		}
	}

	var result string

	// append the ones without names
	for _, d := range nameless {
		result += fmt.Sprintf("%s\n", d)
	}

	// append the ones with names

	// do in sorted order
	var guest_list []string

	for g := range withguest {
		guest_list = append(guest_list, g)
	}
	sort.Strings(guest_list)

	var named_strings []string

	for _, guest := range guest_list {
		agg := withguest[guest]

		for _, ag := range agg {

			var s string

			s += guest

			var host_list []string

			for h := range ag.Hosts {
				host_list = append(host_list, h)
			}
			sort.Strings(host_list)

			hosts := strings.Join(host_list, ",")

			if hosts != "" {
				s += fmt.Sprintf(" [%s]", hosts)
			}

			s += fmt.Sprintf(": %s", ag.Diagnostic)

			named_strings = append(named_strings, s)
		}
	}

	sort.Strings(named_strings)

	result += strings.Join(named_strings, "\n")

	return result

}

func (ds Diagnostics) Equals(other Diagnostics) bool {
	h1 := make([]Hashable, len(ds))
	h2 := make([]Hashable, len(other))

	for i := range ds {
		h1[i] = ds[i]
	}

	for i := range other {
		h2[i] = other[i]
	}

	return HashSliceEquals(h1, h2)
}

func (ds Diagnostics) Dump() {
	fmt.Printf(ds.ToString())
}

func (d Diagnostic) Show() string {

	return fmt.Sprintf("%s %s", d.Message, d.Data)

}

func (f Fields) String() string {

	var fields []string

	for k, v := range f {
		fields = append(fields, fmt.Sprintf("%s:%v", k, v))
	}

	sort.Strings(fields)

	return strings.Join(fields, " ")

}

func (l DiagnosticLevel) Show() string {

	switch l {
	case DiagnosticLevel_Error:
		return "error"
	case DiagnosticLevel_Warning:
		return "warn"
	case DiagnosticLevel_Info:
		return "info"
	case DiagnosticLevel_Debug:
		return "debug"
	case DiagnosticLevel_Trace:
		return "trace"
	default:
		return "?"
	}

}

func (d *Diagnostic) Hash() uint32 {
	h := fnv.New32a()

	if d == nil {
		return h.Sum32()
	}

	bs := make([]byte, 4)

	h.Write([]byte(d.Message))
	binary.LittleEndian.PutUint32(bs, uint32(d.Level))
	h.Write(bs)

	keys := make([]string, 0, len(d.Data))
	for key := range d.Data {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	for _, key := range keys {
		h.Write([]byte(key))
		h.Write([]byte(d.Data[key]))
	}

	h.Write([]byte(d.Guest))
	h.Write([]byte(d.Host))

	return h.Sum32()
}

func (ep *Endpoint) Hash() uint32 {
	h := fnv.New32a()

	if ep == nil {
		return h.Sum32()
	}

	bs := make([]byte, 4)

	// all endpoints
	h.Write([]byte(ep.GetHost()))
	h.Write([]byte(ep.GetMac()))

	// phy
	h.Write([]byte(ep.GetPhy().GetName()))

	// vlan
	h.Write([]byte(ep.GetVlan().GetName()))
	binary.LittleEndian.PutUint32(bs, ep.GetVlan().GetVid())
	h.Write(bs)
	h.Write([]byte(ep.GetVlan().GetParent().GetName()))

	// vtep
	h.Write([]byte(ep.GetVtep().GetName()))
	binary.LittleEndian.PutUint32(bs, ep.GetVtep().GetVni())
	h.Write(bs)
	h.Write([]byte(ep.GetVtep().GetParent().GetName()))
	h.Write([]byte(ep.GetVtep().GetTunnelIp()))

	return h.Sum32()

}

func (wp *Waypoint) Hash() uint32 {
	h := fnv.New32a()

	if wp == nil {
		return h.Sum32()
	}

	bs := make([]byte, 4)

	// all waypoints
	h.Write([]byte(wp.GetHost()))

	// access
	h.Write([]byte(wp.GetAccess().GetPort().GetName()))
	binary.LittleEndian.PutUint32(bs, wp.GetAccess().GetVid())
	h.Write(bs)

	// trunk
	h.Write([]byte(wp.GetTrunk().GetPort().GetName()))
	for _, v := range wp.GetTrunk().GetVids() {
		binary.LittleEndian.PutUint32(bs, v)
		h.Write(bs)
	}

	// vtep
	h.Write([]byte(wp.GetVtep().GetName()))
	binary.LittleEndian.PutUint32(bs, wp.GetVtep().GetVni())
	h.Write(bs)
	h.Write([]byte(wp.GetVtep().GetParent().GetName()))
	h.Write([]byte(wp.GetVtep().GetTunnelIp()))

	// peer
	binary.LittleEndian.PutUint32(bs, wp.GetBgpPeer().GetRemoteAsn())
	h.Write(bs)
	h.Write([]byte(wp.GetBgpPeer().GetInterface().GetName()))
	h.Write([]byte(wp.GetBgpPeer().GetNetwork()))

	return h.Sum32()
}

func (pi *PhysicalInterface) Equals(x *PhysicalInterface) bool {

	if (pi != nil && x == nil) || (pi == nil && x != nil) {
		return false
	}

	if pi == nil && x == nil {
		return true
	}

	return pi.Name == x.Name && pi.Mac == x.Mac
}

func (ep *Endpoint) Equals(other interface{}) bool {
	x, ok := other.(*Endpoint)

	if !ok {
		return false
	}

	if (ep != nil && x == nil) || (ep == nil && x != nil) {
		return false
	}

	if ep == nil && x == nil {
		return true
	}

	if (ep.Host != x.Host) || (ep.Mac != x.Mac) {
		return false
	}

	switch ep.Interface.(type) {
	case *Endpoint_Phy:
		if x.GetPhy() == nil {
			return false
		}

		return ep.GetPhy().Equals(x.GetPhy()) &&
			ep.Virtual == x.Virtual

	case *Endpoint_Vlan:
		if x.GetVlan() == nil {
			return false
		}

		return ep.GetVlan().Name == x.GetVlan().Name &&
			ep.GetVlan().Vid == x.GetVlan().Vid &&
			ep.GetVlan().Parent.Equals(x.GetVlan().Parent)

	case *Endpoint_Vtep:
		if x.GetVtep() == nil {
			return false
		}

		return ep.GetVtep().Name == x.GetVtep().Name &&
			ep.GetVtep().Vni == x.GetVtep().Vni &&
			ep.GetVtep().TunnelIp == x.GetVtep().TunnelIp &&
			ep.GetVtep().Parent.Equals(x.GetVtep().Parent)

	default:
		log.Println("Endpoint Equals: unknown endpoint type")
		return false
	}

	return true
}

func (wp *Waypoint) Equals(other interface{}) bool {
	x, ok := other.(*Waypoint)

	if !ok {
		return false
	}

	if (wp != nil && x == nil) || (wp == nil && x != nil) {
		return false
	}

	if wp == nil && x == nil {
		return true
	}

	if wp.Host != x.Host {
		return false
	}

	switch wp.Interface.(type) {
	case *Waypoint_Access:
		if x.GetAccess() == nil {
			return false
		}
		return wp.GetAccess().Port.GetName() == x.GetAccess().Port.GetName() &&
			wp.GetAccess().Vid == x.GetAccess().Vid

	case *Waypoint_Tap:
		if x.GetTap() == nil {
			return false
		}

		xtap := x.GetTap()
		wtap := wp.GetTap()
		xfront := xtap.GetFrontend()
		wfront := wtap.GetFrontend()

		return xtap.GetNode() == wtap.GetNode() &&
			xtap.GetVid() == wtap.GetVid() &&
			xtap.GetVni() == wtap.GetVni() &&
			xfront.Equals(wfront)

	case *Waypoint_Trunk:
		if x.GetTrunk() == nil {
			return false
		}
		if !wp.GetTrunk().Port.Equals(x.GetTrunk().Port) {
			return false
		}
		for _, t := range wp.GetTrunk().Vids {
			found := false
			for _, tt := range x.GetTrunk().Vids {
				if t == tt {
					found = true
					break
				}
			}
			if !found {
				return false
			}
		}

	case *Waypoint_Vtep:
		if x.GetVtep() == nil {
			return false
		}
		return wp.GetVtep().Name == x.GetVtep().Name &&
			wp.GetVtep().Vni == x.GetVtep().Vni &&
			wp.GetVtep().Parent.Equals(x.GetVtep().Parent)

	case *Waypoint_BgpPeer:
		if x.GetBgpPeer() == nil {
			return false
		}
		return wp.GetBgpPeer().RemoteAsn == x.GetBgpPeer().RemoteAsn &&
			wp.GetBgpPeer().LocalAsn == x.GetBgpPeer().LocalAsn &&
			wp.GetBgpPeer().Network == x.GetBgpPeer().Network &&
			wp.GetBgpPeer().Interface.Equals(x.GetBgpPeer().Interface)

	default:
		log.Println("Waypoint Equals: unknown waypoint type")
		return false
	}

	return true

}

func (d *Diagnostic) Equals(other interface{}) bool {
	x, ok := other.(*Diagnostic)

	if !ok {
		return false
	}

	if (d != nil && x == nil) || (d == nil && x != nil) {
		return false
	}

	if d == nil && x == nil {
		return true
	}

	return d.Message == x.Message &&
		d.Level == x.Level &&
		d.Guest == x.Guest &&
		reflect.DeepEqual(d.Data, x.Data)
}

func (seg *LinkSegment) Equals(other *LinkSegment) bool {

	if (seg != nil && other == nil) || (seg == nil && other != nil) {
		return false
	}

	if seg == nil && other == nil {
		return true
	}

	h1 := make([]Hashable, len(seg.Endpoints)+len(seg.Waypoints))

	for i := range seg.Endpoints {
		h1[i] = seg.Endpoints[i]
	}
	for i := range seg.Waypoints {
		h1[i+len(seg.Endpoints)] = seg.Waypoints[i]
	}

	h2 := make([]Hashable, len(other.Endpoints)+len(other.Waypoints))

	for i := range other.Endpoints {
		h2[i] = other.Endpoints[i]
	}
	for i := range other.Waypoints {
		h2[i+len(other.Endpoints)] = other.Waypoints[i]
	}

	return HashSliceEquals(h1, h2)
}

func (seg *LinkSegment) ToString(indent string, lsi uint64) string {

	if seg == nil {
		return "nil"
	}

	s := indent + "ENDPOINTS:\n"
	for _, ep := range seg.Endpoints {
		bridge := fmt.Sprintf(" {%+v}", ep.Bridge)
		if ep.Bridge == nil {
			bridge = ""
		}

		s += indent + indent + fmt.Sprintf("[%d] %s@%s %+v%s\n", lsi, ep.Node, ep.Host, ep.Interface, bridge)
	}
	s += indent + "WAYPOINTS:\n"
	for _, wp := range seg.Waypoints {
		bridge := fmt.Sprintf(" {%+v}", wp.Bridge)
		if wp.Bridge == nil {
			bridge = ""
		}

		s += indent + indent + fmt.Sprintf("[%d] %s: %+v%s\n", lsi, wp.Host, wp.Interface, bridge)
	}

	return s
}

func (lrz *LinkRealization) Equals(other *LinkRealization) bool {
	if (lrz != nil && other == nil) || (lrz == nil && other != nil) {
		return false
	}

	if lrz == nil && other == nil {
		return true
	}

	if len(lrz.Segments) != len(other.Segments) {
		return false
	}

	for id, s_seg := range lrz.Segments {
		o_seg, ok := other.Segments[id]

		if !ok || !s_seg.Equals(o_seg) {
			return false
		}
	}

	return true
}

func (lrz *LinkRealization) ToString(indent string) string {

	if lrz == nil {
		return "nil"
	}

	// Sort ids

	ids := make([]uint64, 0, len(lrz.Segments))
	for k := range lrz.Segments {
		ids = append(ids, k)
	}
	sort.Slice(ids,
		func(i, j int) bool {
			return ids[i] > ids[j]
		},
	)

	// Actually print the ids

	s := ""
	for _, lsi := range ids {
		s += lrz.Segments[lsi].ToString(indent, lsi)
	}
	return s

}

func (rz *Realization) RemoveDuplicateWaypoints() {

	if rz == nil {
		return
	}

	added_waypoints := make(Set)

	for _, link := range rz.Links {
		for _, seg := range link.Segments {

			// Waypoints
			h := make([]Hashable, len(seg.Waypoints))
			for i := range h {
				h[i] = seg.Waypoints[i]
			}

			h, added_waypoints = RemoveDuplicates(h, added_waypoints)

			seg.Waypoints = make([]*Waypoint, len(h))
			for i := range h {
				val, _ := h[i].(*Waypoint)
				seg.Waypoints[i] = val
			}
		}
	}
}

func (rz *Realization) ToString() string {

	if rz == nil {
		return "nil"
	}

	// Print Nodes

	nodes := make([]string, 0, len(rz.Nodes))
	for n := range rz.Nodes {
		nodes = append(nodes, n)
	}
	sort.Strings(nodes)

	s := fmt.Sprintf("NODES (%d)\n", len(rz.Nodes))

	for _, xnode := range nodes {
		rnode := rz.Nodes[xnode]
		s += fmt.Sprintf("%s -> %s@%s (%s)\n", xnode, rnode.Resource.Id, rnode.Facility, rnode.Kind)
	}

	// Print Links

	found_infranet := false
	found_harbornet := false
	links := make([]string, 0, len(rz.Links))

	for l := range rz.Links {
		switch l {
		case "infranet":
			found_infranet = true
		case "harbor":
			found_harbornet = true
		default:
			links = append(links, l)
		}
	}
	sort.Strings(links)

	// Put the infranet links and harbor links at the end
	if found_infranet {
		links = append(links, "infranet")
	}

	if found_harbornet {
		links = append(links, "harbor")
	}

	s += fmt.Sprintf("LINKS (%d)\n", len(rz.Links))

	for _, xlink := range links {
		s += fmt.Sprintln(xlink)
		s += rz.Links[xlink].ToString("  ")
	}

	return s

}
