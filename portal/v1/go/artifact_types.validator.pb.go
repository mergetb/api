// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: portal/v1/artifact_types.proto

package portal

import (
	fmt "fmt"
	math "math"
	proto "github.com/golang/protobuf/proto"
	_ "google.golang.org/protobuf/types/known/emptypb"
	_ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2/options"
	_ "github.com/mwitkow/go-proto-validators"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	regexp "regexp"
	github_com_mwitkow_go_proto_validators "github.com/mwitkow/go-proto-validators"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

var _regex_ArtifactReadme_Name = regexp.MustCompile(`^((https?|HTTPS?)[^ ]{5,100})|([a-zA-Z0-9_.-]{0,32})$`)

func (this *ArtifactReadme) Validate() error {
	if !_regex_ArtifactReadme_Name.MatchString(this.Name) {
		return github_com_mwitkow_go_proto_validators.FieldError("Name", fmt.Errorf(`value '%v' must be a string conforming to regex "^((https?|HTTPS?)[^ ]{5,100})|([a-zA-Z0-9_.-]{0,32})$"`, this.Name))
	}
	if !(len(this.Content) < 204800) {
		return github_com_mwitkow_go_proto_validators.FieldError("Content", fmt.Errorf(`value '%v' must have a length smaller than '204800'`, this.Content))
	}
	return nil
}

var _regex_ArtifactId_Uuid = regexp.MustCompile(`^([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[4][a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12})?$`)

func (this *ArtifactId) Validate() error {
	if !_regex_ArtifactId_Uuid.MatchString(this.Uuid) {
		return github_com_mwitkow_go_proto_validators.FieldError("Uuid", fmt.Errorf(`value '%v' must be a string conforming to regex "^([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[4][a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12})?$"`, this.Uuid))
	}
	if this.Uuid == "" {
		return github_com_mwitkow_go_proto_validators.FieldError("Uuid", fmt.Errorf(`value '%v' must not be an empty string`, this.Uuid))
	}
	return nil
}

var _regex_ArtifactMetadata_Name = regexp.MustCompile(`^[^ ]{1,32}$`)
var _regex_ArtifactMetadata_Checksum = regexp.MustCompile(`^SHA1:[0-9a-f]{40}|SHA256:[0-9a-f]{64}|$`)
var _regex_ArtifactMetadata_Description = regexp.MustCompile(`^.{10,120}$`)
var _regex_ArtifactMetadata_Project = regexp.MustCompile(`^(|[a-z][0-9a-z]{0,31})$`)
var _regex_ArtifactMetadata_Organization = regexp.MustCompile(`^(|[A-Za-z0-9][A-Za-z0-9 -]{0,62})$`)
var _regex_ArtifactMetadata_Keywords = regexp.MustCompile(`^[a-z0-9_.-]{1,30}$`)

func (this *ArtifactMetadata) Validate() error {
	if this.Id != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Id); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Id", err)
		}
	}
	if !_regex_ArtifactMetadata_Name.MatchString(this.Name) {
		return github_com_mwitkow_go_proto_validators.FieldError("Name", fmt.Errorf(`value '%v' must be a string conforming to regex "^[^ ]{1,32}$"`, this.Name))
	}
	if this.Readme != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Readme); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Readme", err)
		}
	}
	if !_regex_ArtifactMetadata_Checksum.MatchString(this.Checksum) {
		return github_com_mwitkow_go_proto_validators.FieldError("Checksum", fmt.Errorf(`value '%v' must be a string conforming to regex "^SHA1:[0-9a-f]{40}|SHA256:[0-9a-f]{64}|$"`, this.Checksum))
	}
	if !_regex_ArtifactMetadata_Description.MatchString(this.Description) {
		return github_com_mwitkow_go_proto_validators.FieldError("Description", fmt.Errorf(`value '%v' must be a string conforming to regex "^.{10,120}$"`, this.Description))
	}
	if !_regex_ArtifactMetadata_Project.MatchString(this.Project) {
		return github_com_mwitkow_go_proto_validators.FieldError("Project", fmt.Errorf(`value '%v' must be a string conforming to regex "^(|[a-z][0-9a-z]{0,31})$"`, this.Project))
	}
	if !_regex_ArtifactMetadata_Organization.MatchString(this.Organization) {
		return github_com_mwitkow_go_proto_validators.FieldError("Organization", fmt.Errorf(`value '%v' must be a string conforming to regex "^(|[A-Za-z0-9][A-Za-z0-9 -]{0,62})$"`, this.Organization))
	}
	for _, item := range this.Keywords {
		if !_regex_ArtifactMetadata_Keywords.MatchString(item) {
			return github_com_mwitkow_go_proto_validators.FieldError("Keywords", fmt.Errorf(`value '%v' must be a string conforming to regex "^[a-z0-9_.-]{1,30}$"`, item))
		}
	}
	return nil
}
func (this *ArtifactListRequest) Validate() error {
	return nil
}
func (this *ArtifactListResponse) Validate() error {
	for _, item := range this.Metadata {
		if item != nil {
			if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(item); err != nil {
				return github_com_mwitkow_go_proto_validators.FieldError("Metadata", err)
			}
		}
	}
	return nil
}
func (this *ArtifactPutRequest) Validate() error {
	if this.Metadata != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Metadata); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Metadata", err)
		}
	}
	return nil
}
func (this *ArtifactPutResponse) Validate() error {
	if this.Metadata != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Metadata); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Metadata", err)
		}
	}
	return nil
}
func (this *ArtifactGetRequest) Validate() error {
	if this.Id != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Id); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Id", err)
		}
	}
	return nil
}
func (this *ArtifactGetResponse) Validate() error {
	if this.Metadata != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Metadata); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Metadata", err)
		}
	}
	return nil
}
func (this *ArtifactDelRequest) Validate() error {
	if this.Id != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Id); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Id", err)
		}
	}
	return nil
}
func (this *ArtifactDelResponse) Validate() error {
	return nil
}
func (this *ArtifactQuota) Validate() error {
	return nil
}
func (this *ArtifactQuotas) Validate() error {
	for _, item := range this.Quotas {
		if item != nil {
			if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(item); err != nil {
				return github_com_mwitkow_go_proto_validators.FieldError("Quotas", err)
			}
		}
	}
	return nil
}
func (this *ArtifactQuotaGetRequest) Validate() error {
	return nil
}
func (this *ArtifactQuotaGetResponse) Validate() error {
	if this.Quota != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Quota); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Quota", err)
		}
	}
	return nil
}
func (this *ArtifactQuotaSetRequest) Validate() error {
	if this.Quota != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Quota); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Quota", err)
		}
	}
	return nil
}
func (this *ArtifactQuotaSetResponse) Validate() error {
	if this.Quota != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.Quota); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("Quota", err)
		}
	}
	return nil
}
func (this *ArtifactQuotaListRequest) Validate() error {
	return nil
}
func (this *ArtifactQuotaListResponse) Validate() error {
	for _, item := range this.Quotas {
		if item != nil {
			if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(item); err != nil {
				return github_com_mwitkow_go_proto_validators.FieldError("Quotas", err)
			}
		}
	}
	return nil
}
