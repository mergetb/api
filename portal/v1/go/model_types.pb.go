// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.31.0
// 	protoc        (unknown)
// source: portal/v1/model_types.proto

package portal

import (
	_ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2/options"
	_ "github.com/mwitkow/go-proto-validators"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CompileRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Model string `protobuf:"bytes,1,opt,name=model,proto3" json:"model,omitempty"`
}

func (x *CompileRequest) Reset() {
	*x = CompileRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_portal_v1_model_types_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CompileRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CompileRequest) ProtoMessage() {}

func (x *CompileRequest) ProtoReflect() protoreflect.Message {
	mi := &file_portal_v1_model_types_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CompileRequest.ProtoReflect.Descriptor instead.
func (*CompileRequest) Descriptor() ([]byte, []int) {
	return file_portal_v1_model_types_proto_rawDescGZIP(), []int{0}
}

func (x *CompileRequest) GetModel() string {
	if x != nil {
		return x.Model
	}
	return ""
}

type CompileResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Success bool     `protobuf:"varint,1,opt,name=success,proto3" json:"success,omitempty"`
	Errors  []string `protobuf:"bytes,2,rep,name=errors,proto3" json:"errors,omitempty"`
	Network string   `protobuf:"bytes,3,opt,name=network,proto3" json:"network,omitempty"`
}

func (x *CompileResponse) Reset() {
	*x = CompileResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_portal_v1_model_types_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CompileResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CompileResponse) ProtoMessage() {}

func (x *CompileResponse) ProtoReflect() protoreflect.Message {
	mi := &file_portal_v1_model_types_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CompileResponse.ProtoReflect.Descriptor instead.
func (*CompileResponse) Descriptor() ([]byte, []int) {
	return file_portal_v1_model_types_proto_rawDescGZIP(), []int{1}
}

func (x *CompileResponse) GetSuccess() bool {
	if x != nil {
		return x.Success
	}
	return false
}

func (x *CompileResponse) GetErrors() []string {
	if x != nil {
		return x.Errors
	}
	return nil
}

func (x *CompileResponse) GetNetwork() string {
	if x != nil {
		return x.Network
	}
	return ""
}

type PushRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Model      string `protobuf:"bytes,1,opt,name=model,proto3" json:"model,omitempty"`
	Branch     string `protobuf:"bytes,2,opt,name=branch,proto3" json:"branch,omitempty"`
	Experiment string `protobuf:"bytes,3,opt,name=experiment,proto3" json:"experiment,omitempty"`
	Project    string `protobuf:"bytes,4,opt,name=project,proto3" json:"project,omitempty"`
	Tag        string `protobuf:"bytes,5,opt,name=tag,proto3" json:"tag,omitempty"`
}

func (x *PushRequest) Reset() {
	*x = PushRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_portal_v1_model_types_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PushRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PushRequest) ProtoMessage() {}

func (x *PushRequest) ProtoReflect() protoreflect.Message {
	mi := &file_portal_v1_model_types_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PushRequest.ProtoReflect.Descriptor instead.
func (*PushRequest) Descriptor() ([]byte, []int) {
	return file_portal_v1_model_types_proto_rawDescGZIP(), []int{2}
}

func (x *PushRequest) GetModel() string {
	if x != nil {
		return x.Model
	}
	return ""
}

func (x *PushRequest) GetBranch() string {
	if x != nil {
		return x.Branch
	}
	return ""
}

func (x *PushRequest) GetExperiment() string {
	if x != nil {
		return x.Experiment
	}
	return ""
}

func (x *PushRequest) GetProject() string {
	if x != nil {
		return x.Project
	}
	return ""
}

func (x *PushRequest) GetTag() string {
	if x != nil {
		return x.Tag
	}
	return ""
}

type PushResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Revision string `protobuf:"bytes,1,opt,name=revision,proto3" json:"revision,omitempty"`
}

func (x *PushResponse) Reset() {
	*x = PushResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_portal_v1_model_types_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PushResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PushResponse) ProtoMessage() {}

func (x *PushResponse) ProtoReflect() protoreflect.Message {
	mi := &file_portal_v1_model_types_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PushResponse.ProtoReflect.Descriptor instead.
func (*PushResponse) Descriptor() ([]byte, []int) {
	return file_portal_v1_model_types_proto_rawDescGZIP(), []int{3}
}

func (x *PushResponse) GetRevision() string {
	if x != nil {
		return x.Revision
	}
	return ""
}

var File_portal_v1_model_types_proto protoreflect.FileDescriptor

var file_portal_v1_model_types_proto_rawDesc = []byte{
	0x0a, 0x1b, 0x70, 0x6f, 0x72, 0x74, 0x61, 0x6c, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x6f, 0x64, 0x65,
	0x6c, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x09, 0x70,
	0x6f, 0x72, 0x74, 0x61, 0x6c, 0x2e, 0x76, 0x31, 0x1a, 0x2b, 0x6d, 0x77, 0x69, 0x74, 0x6b, 0x6f,
	0x77, 0x2f, 0x67, 0x6f, 0x2d, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2d, 0x76, 0x61, 0x6c, 0x69, 0x64,
	0x61, 0x74, 0x6f, 0x72, 0x73, 0x2f, 0x76, 0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x6f, 0x72, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63, 0x2d, 0x67, 0x65,
	0x6e, 0x2d, 0x6f, 0x70, 0x65, 0x6e, 0x61, 0x70, 0x69, 0x76, 0x32, 0x2f, 0x6f, 0x70, 0x74, 0x69,
	0x6f, 0x6e, 0x73, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x5b, 0x0a, 0x0e, 0x43, 0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x65,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x49, 0x0a, 0x05, 0x6d, 0x6f, 0x64, 0x65, 0x6c,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x42, 0x33, 0x92, 0x41, 0x2a, 0x32, 0x28, 0x54, 0x68, 0x65,
	0x20, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x20, 0x63, 0x6f, 0x64, 0x65, 0x20, 0x6f, 0x66, 0x20,
	0x74, 0x68, 0x65, 0x20, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x20, 0x74, 0x6f, 0x20, 0x63, 0x6f, 0x6d,
	0x70, 0x69, 0x6c, 0x65, 0x2e, 0xe2, 0xdf, 0x1f, 0x02, 0x58, 0x01, 0x52, 0x05, 0x6d, 0x6f, 0x64,
	0x65, 0x6c, 0x22, 0xb3, 0x01, 0x0a, 0x0f, 0x43, 0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x65, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73,
	0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x07, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73,
	0x12, 0x39, 0x0a, 0x06, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x09,
	0x42, 0x21, 0x92, 0x41, 0x1e, 0x32, 0x1c, 0x53, 0x74, 0x61, 0x63, 0x6b, 0x20, 0x6f, 0x66, 0x20,
	0x63, 0x6f, 0x6d, 0x70, 0x69, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x20, 0x65, 0x72, 0x72, 0x6f,
	0x72, 0x73, 0x2e, 0x52, 0x06, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x73, 0x12, 0x4b, 0x0a, 0x07, 0x6e,
	0x65, 0x74, 0x77, 0x6f, 0x72, 0x6b, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x42, 0x31, 0x92, 0x41,
	0x2e, 0x32, 0x2c, 0x54, 0x68, 0x65, 0x20, 0x6e, 0x65, 0x74, 0x77, 0x6f, 0x72, 0x6b, 0x20, 0x69,
	0x73, 0x20, 0x61, 0x20, 0x62, 0x61, 0x73, 0x65, 0x36, 0x34, 0x20, 0x65, 0x6e, 0x63, 0x6f, 0x64,
	0x65, 0x64, 0x20, 0x58, 0x49, 0x52, 0x20, 0x6e, 0x65, 0x74, 0x77, 0x6f, 0x72, 0x6b, 0x2e, 0x52,
	0x07, 0x6e, 0x65, 0x74, 0x77, 0x6f, 0x72, 0x6b, 0x22, 0x82, 0x03, 0x0a, 0x0b, 0x50, 0x75, 0x73,
	0x68, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x49, 0x0a, 0x05, 0x6d, 0x6f, 0x64, 0x65,
	0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x42, 0x33, 0x92, 0x41, 0x2a, 0x32, 0x28, 0x54, 0x68,
	0x65, 0x20, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x20, 0x63, 0x6f, 0x64, 0x65, 0x20, 0x6f, 0x66,
	0x20, 0x74, 0x68, 0x65, 0x20, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x20, 0x74, 0x6f, 0x20, 0x63, 0x6f,
	0x6d, 0x70, 0x69, 0x6c, 0x65, 0x2e, 0xe2, 0xdf, 0x1f, 0x02, 0x58, 0x01, 0x52, 0x05, 0x6d, 0x6f,
	0x64, 0x65, 0x6c, 0x12, 0x5b, 0x0a, 0x06, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x42, 0x43, 0x92, 0x41, 0x40, 0x32, 0x3e, 0x54, 0x68, 0x65, 0x20, 0x62, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x65, 0x78, 0x70, 0x65,
	0x72, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x20, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x6f,
	0x72, 0x79, 0x20, 0x74, 0x6f, 0x20, 0x70, 0x75, 0x73, 0x68, 0x20, 0x74, 0x68, 0x65, 0x20, 0x6d,
	0x6f, 0x64, 0x65, 0x6c, 0x20, 0x74, 0x6f, 0x2e, 0x52, 0x06, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68,
	0x12, 0x3f, 0x0a, 0x0a, 0x65, 0x78, 0x70, 0x65, 0x72, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x42, 0x1f, 0x92, 0x41, 0x16, 0x32, 0x14, 0x54, 0x68, 0x65, 0x20, 0x65,
	0x78, 0x70, 0x65, 0x72, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x20, 0x6e, 0x61, 0x6d, 0x65, 0x2e, 0xe2,
	0xdf, 0x1f, 0x02, 0x58, 0x01, 0x52, 0x0a, 0x65, 0x78, 0x70, 0x65, 0x72, 0x69, 0x6d, 0x65, 0x6e,
	0x74, 0x12, 0x36, 0x0a, 0x07, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x18, 0x04, 0x20, 0x01,
	0x28, 0x09, 0x42, 0x1c, 0x92, 0x41, 0x13, 0x32, 0x11, 0x54, 0x68, 0x65, 0x20, 0x70, 0x72, 0x6f,
	0x6a, 0x65, 0x63, 0x74, 0x20, 0x6e, 0x61, 0x6d, 0x65, 0x2e, 0xe2, 0xdf, 0x1f, 0x02, 0x58, 0x01,
	0x52, 0x07, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x12, 0x52, 0x0a, 0x03, 0x74, 0x61, 0x67,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x42, 0x40, 0x92, 0x41, 0x3d, 0x32, 0x3b, 0x4f, 0x70, 0x74,
	0x69, 0x6f, 0x6e, 0x61, 0x6c, 0x20, 0x74, 0x61, 0x67, 0x2e, 0x20, 0x49, 0x66, 0x20, 0x67, 0x69,
	0x76, 0x65, 0x6e, 0x2c, 0x20, 0x74, 0x61, 0x67, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x70, 0x75,
	0x73, 0x68, 0x20, 0x77, 0x69, 0x74, 0x68, 0x20, 0x74, 0x68, 0x65, 0x20, 0x67, 0x69, 0x76, 0x65,
	0x6e, 0x20, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x2e, 0x52, 0x03, 0x74, 0x61, 0x67, 0x22, 0x2a, 0x0a,
	0x0c, 0x50, 0x75, 0x73, 0x68, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x1a, 0x0a,
	0x08, 0x72, 0x65, 0x76, 0x69, 0x73, 0x69, 0x6f, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x08, 0x72, 0x65, 0x76, 0x69, 0x73, 0x69, 0x6f, 0x6e, 0x42, 0x2c, 0x5a, 0x2a, 0x67, 0x69, 0x74,
	0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x6d, 0x65, 0x72, 0x67, 0x65, 0x74, 0x62, 0x2f,
	0x61, 0x70, 0x69, 0x2f, 0x70, 0x6f, 0x72, 0x74, 0x61, 0x6c, 0x2f, 0x76, 0x31, 0x2f, 0x67, 0x6f,
	0x3b, 0x70, 0x6f, 0x72, 0x74, 0x61, 0x6c, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_portal_v1_model_types_proto_rawDescOnce sync.Once
	file_portal_v1_model_types_proto_rawDescData = file_portal_v1_model_types_proto_rawDesc
)

func file_portal_v1_model_types_proto_rawDescGZIP() []byte {
	file_portal_v1_model_types_proto_rawDescOnce.Do(func() {
		file_portal_v1_model_types_proto_rawDescData = protoimpl.X.CompressGZIP(file_portal_v1_model_types_proto_rawDescData)
	})
	return file_portal_v1_model_types_proto_rawDescData
}

var file_portal_v1_model_types_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_portal_v1_model_types_proto_goTypes = []interface{}{
	(*CompileRequest)(nil),  // 0: portal.v1.CompileRequest
	(*CompileResponse)(nil), // 1: portal.v1.CompileResponse
	(*PushRequest)(nil),     // 2: portal.v1.PushRequest
	(*PushResponse)(nil),    // 3: portal.v1.PushResponse
}
var file_portal_v1_model_types_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_portal_v1_model_types_proto_init() }
func file_portal_v1_model_types_proto_init() {
	if File_portal_v1_model_types_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_portal_v1_model_types_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CompileRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_portal_v1_model_types_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CompileResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_portal_v1_model_types_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PushRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_portal_v1_model_types_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PushResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_portal_v1_model_types_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_portal_v1_model_types_proto_goTypes,
		DependencyIndexes: file_portal_v1_model_types_proto_depIdxs,
		MessageInfos:      file_portal_v1_model_types_proto_msgTypes,
	}.Build()
	File_portal_v1_model_types_proto = out.File
	file_portal_v1_model_types_proto_rawDesc = nil
	file_portal_v1_model_types_proto_goTypes = nil
	file_portal_v1_model_types_proto_depIdxs = nil
}
